//
//  GameViewController.swift
//  SALVAGENTE
//
//  Created by Delia Cavalli on 21/11/2019.
//  Copyright © 2019 Delia Cavalli. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit

class GameViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBOutlet weak var asteroid1: UIImageView!
    
    @IBOutlet weak var ufo: UIImageView!
       
       var yDistance = 0
       var xDistance = 0
       var isYMovement = true

    
    @IBAction func buttonUp(_ sender: Any) {
        
        if (isYMovement && yDistance > -230) {
            yDistance = yDistance - 115
        }
        movement()
    }
    
    @IBAction func buttonDown(_ sender: Any) {
        
        if (isYMovement && yDistance < 0){
            yDistance = yDistance + 115
        }
        movement()
    }
    
    @IBAction func buttonLeft(_ sender: Any) {
    
        if (isYMovement && xDistance > -230){
            xDistance = xDistance - 115
        }
        movement()
    }
    
    @IBAction func buttonRight(_ sender: Any) {
            
        if (isYMovement && xDistance < 0){
           xDistance = xDistance + 115
        }
        movement()
    }
    
    
    func movement() {
        UIView.animate(withDuration: 0.5) {
            self.ufo.transform = CGAffineTransform(translationX: CGFloat(self.xDistance), y: CGFloat(self.yDistance))
            
        }
        
        func collision(){
    }
  }
}
